function scores = matcaffe(im, useGpu, modelPrototxt, modelFile)
% MATCAFFE Modified Matlab wrapper for Caffe evaluation
% Demo of the matlab wrapper based on the networks used for the "VGG" entry
% in the ILSVRC-2014 competition and described in the tech. report 
% "Very Deep Convolutional Networks for Large-Scale Image Recognition"
% http://arxiv.org/abs/1409.1556/
% 
% scores = matcaffe(im, useGpu, modelPrototxt, modelFile)
%
% INPUT
%   im:            color image as uint8 HxWx3
%   useGpu:        1 to use the GPU, 0 to use the CPU
%   modelPrototxt: network configuration (.prototxt file)
%   modelFile:     network weights (.caffemodel file)
%
% OUTPUT
%   scores:  Output of the last layer of the model defined in modelPrototxt
%
% EXAMPLE USAGE
%  modelPrototxt = 'zoo/deploy.prototxt';
%  modelFile = 'zoo/model.caffemodel';
%  useGpu = true;
%  im = imread('../../examples/images/cat.jpg');
%  scores = matcaffe(im, iseGpu, modelPrototxt, modelFile);
% 
% NOTES
%  mean pixel subtraction is used instead of the mean image subtraction
%
% PREREQUISITES
%  You may need to do the following before you start matlab:
%   $ export LD_LIBRARY_PATH=/opt/intel/mkl/lib/intel64:/usr/local/cuda/lib64
%   $ export LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6
%  Or the equivalent based on where things are installed on your system
% 
% Stavros Tsogkas, <stavros.tsogkas@centralesupelec.fr>
% Last update: March 2015 

if nargin < 2, useGpu = 1; end % by default use gpu
if nargin > 2
    caffe('reset');
    matcaffe_init(useGpu,modelPrototxt,modelFile); 
end

sizeNorm = [321,321]; % default image size (depends on the trained model)
im = readInputImages(im, sizeNorm);

% do forward pass to get scores
scores = caffe('forward', {im});

% Permute again before sending to output
scores = permute(squeeze(scores{1}), [2 1 3 4]);


function im = readInputImages(input, sizeNorm)
% Read images from input, cast to float and resize them to fixed size

if isnumeric(input)   % Read input images
    sz = size(input);
    if ismatrix(input)  % if grayscale, turn to RGB
        input = repmat(input, [1 1 3]); 
    end 
    if isequal(sz(1:2),sizeNorm)
        im = single(input);
    else
        nImages = size(input,4);
        im = zeros([sizeNorm, 3, nImages],'single');
        for i=1:nImages
            im(:,:,:,i) = imresize(single(input(:,:,:,i)),sizeNorm,'bilinear');
        end
    end
elseif iscell(input)   % Read from a cell array
    im = zeros(sizeNorm(1),sizeNorm(2),3,numel(input),'single');
    for i=1:numel(input)
        currentImage = single(input{i}); sz = size(currentImage);
        if ~isequal(sz(1:2),sizeNorm)
            currentImage = imresize(currentImage,sizeNorm,'bilinear'); 
        end
        im(:,:,:,i) = currentImage;
    end
elseif ischar(input) && isdir(input)    % Read all images in a directory
    images = [dir([input '/*.jpg']); dir([input '/*.png'])];
    im = zeros(sizeNorm(1),sizeNorm(2),3,numel(images),'single');
    for i=1:numel(images)
        currentImage = single(imread(images(i).name)); sz = size(currentImage);
        if ismatrix(currentImage)  % if grayscale, turn to RGB
            input = repmat(input, [1 1 3]); 
        end 
        if ~isequal(sz(1:2),sizeNorm)
            currentImage = imresize(currentImage,sizeNorm,'bilinear');
        end
        im(:,:,:,i) = currentImage; 
    end
else
    error('Invalid input')
end

% Transform input images to caffe format (Width x Height x Channels x Num)
% and subtract mean pixel.
im = permute(im(:,:,[3,2,1],:),[2 1 3 4]);
meanPixelBGR = [103.939, 116.779, 123.68];
for c=1:3
    im(:,:,c,:) = im(:,:,c,:) - meanPixelBGR(c); 
end
