function im = preprocess(im,medfiltKernel)
% Filters and adjusts image contrast

if nargin < 2, medfiltKernel = [9 9]; end
im = medfilt2(im,medfiltKernel);  % denoise image
im = double(im); 
im = uint8(im/max(im(:))*255); % deepseg deemands uint8 as input
