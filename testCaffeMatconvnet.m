%% Read image
im = imread('/home/tsogkas/code/partseg/groundtruth/partImages/person/train/2008_000008_person_1_img.jpg');


%% Evaluate Caffe
paths = setPaths();
useGpu = 0;
modelProto = fullfile(paths.protos, 'matcaffe_person.prototxt');
modelFile  = fullfile(paths.models, 'vgg128_noup_pool3_trainval_person_iter_6000.caffemodel');
caffe('release'); matcaffe_init(useGpu,modelProto,modelFile);
rescaffe = matcaffe(im,useGpu);
pcaffe = softmax(rescaffe);

%% Evaluate Matconvnet
tmp = load('models/person_partseg.mat');
net.layers = tmp.layers; net.normalization = tmp.normalization;
im = cnnNormalizeIm(im,net);
net = vl_simplenn_move(net,'gpu');
resmatconv = vl_simplenn(net,gpuArray(im));
pmatconv = softmax(resmatconv(end).x);




