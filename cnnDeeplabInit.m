function net = cnnDeeplabInit(varargin)
% CNN_IMAGENET_INIT  Baseline CNN model

opts.scale = 1 ;
opts.initBias = 0.1 ;
opts.weightDecay = 1 ;
opts.nLabels = 4;
opts = vl_argparse(opts, varargin) ;

% Load pretrained network, define input and output size
net = initializePretrainedModel('faces'); n_0 = numel(net.layers);
net.normalization.imageSize = [321, 321, 1] ;
net.normalization.interpolation = 'bilinear' ;
net.normalization.averageImage = [] ;
net.normalization.keepAspect = true ;
net.backPropDepth = 1;
net.outputSize = [41 41]; % STATSOGK: height and width of the final convolutional layer

% 41 x 41 x 4096 --> 41 x 41 x opts.nLabels
net.layers{end+1} = struct('type', 'conv', 'name', 'fc8', ...
                           'weights', {{0.01/opts.scale * ...
                           randn(1, 1, 4096, opts.nLabels, 'single'), ...
                           zeros(1, opts.nLabels, 'single')}}, ... % biases
                           'stride', 1, ...
                           'pad', 0, ...
                           'hole', 1,...
                           'learningRate', [1 2], ...
                           'weightDecay', [opts.weightDecay 0]) ; 

% LOSS BLOCK
net.layers{end+1} = struct('type', 'softmaxloss', 'name', 'loss') ;
net.backPropDepth = numel(net.layers) - n_0;


function net = initializePretrainedModel(objectClass)
% Load converted deeplab model, remove last layer and freeze learning rates 
% for the remaining layers to perform finetuning on the new layers.
paths = setPaths(); 
if nargin < 1 || isempty(objectClass)
    tmp = load(fullfile(paths.models, 'vgg16_128.caffemodel'));
else
    tmp = load(fullfile(paths.models, [objectClass '_partseg.mat']));
end
net.layers = tmp.layers; net.normalization = tmp.normalization;
net.layers(end) = [];  % throw away last layer (classification layer in partseg code)
% for i=1:numel(net.layers)
%     net.layers{i}.learningRate = zeros(1,2,'single');
% end
