function paths = setPaths()

paths.root         = '/home/tsogkas/code/ffoct';
paths.results      = fullfile(paths.root,'results');
paths.protos       = fullfile(paths.root,'protos');
paths.lists        = fullfile(paths.root,'lists');
paths.models       = fullfile(paths.root,'models');
paths.data         = fullfile(paths.root, 'data');
paths.images       = fullfile(paths.data, 'Image');
paths.background   = fullfile(paths.data, 'background');
paths.colmod       = fullfile(paths.data, 'colmod');
paths.cis          = fullfile(paths.data, 'cis');
paths.isi          = fullfile(paths.data, 'isi');
paths.lis          = fullfile(paths.data, 'lis');
paths.labels       = fullfile(paths.data, 'labels');
paths.labelPatches = fullfile(paths.data, 'label_patches');
paths.imagePatches = fullfile(paths.data, 'image_patches');

% medical datasets
paths.medical         = fullfile('/home/tsogkas/datasets/medical');
paths.isbr.images     = fullfile(paths.data, 'isbr_images');
paths.isbr.labels     = fullfile(paths.data, 'isbr_labels');
paths.isbr.data       = fullfile(paths.medical, 'isbr');
paths.isbr_new.images = fullfile(paths.medical, 'isbr-new','orig');
paths.isbr_new.labels = fullfile(paths.medical, 'isbr-new','seg');
paths.ORL_orig        = fullfile(paths.medical, 'ORL_orig');
paths.ORL_parotidlr   = fullfile(paths.medical, 'ORL_parotidlr');
paths.MTL.images      = fullfile(paths.medical, 'MTL', 'orig_af_crop');
paths.MTL.labels      = fullfile(paths.medical, 'MTL', 'label_afcrop_edited01');