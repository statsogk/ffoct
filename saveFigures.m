function saveFigures(figIndex)

if nargin < 1, figIndex = 1; end

warning off;
paths  = setPaths();
data   = loadData();
images = data.images;
labels = data.labels;
[h,w,nSlices,nVolumes] = size(images);
tmp = load('pascal_seg_colormap'); cmap = tmp.colormap;
savePath = fullfile(paths.results, 'cnn','isbr'); mkdir(savePath);
switch figIndex
    case 1
        sliceStart = 25; sliceEnd = 120;
        for v=1:nVolumes
            tmp = load(fullfile(savePath, ['IBSR_res' num2str(v) '.mat']));
            res = tmp.res(:,:,sliceStart:sliceEnd);
            lbl = labels(:,:,sliceStart:sliceEnd,v);
            fgt = figure; montageArray(lbl,cmap);
            fcnn= figure; montageArray(res,cmap);
            export_fig(fullfile(savePath,'figures',['montage_gt'  num2str(v)]),'-pdf','-transparent','-r300',fgt);
            export_fig(fullfile(savePath,'figures',['montage_cnn' num2str(v)]),'-pdf','-transparent','-r300',fcnn);
        end     
    case 2  % 
        labelsISBI = [10,11,12,13,49,50,51,52]; nLabels = numel(labelsISBI);
%         labelsCVPR = [4,10,11,12,13,17,18,26,43,49,50,51,52,53,54,58];
        valid = labels == 10 | labels == 11 | labels == 12 | labels == 13 ...
              | labels == 49 | labels == 50 | labels == 51 | labels == 52;
        labels(~valid) = 0;
        for v=1:nVolumes
            % pick a slice somewhere in the middle (from a total of 145)
            tmp = load(fullfile(savePath, ['IBSR_res' num2str(v) '.mat']));
            indSlice = 80;
            res = tmp.res(:,:,indSlice); 
            lbl = labels(:,:,indSlice,v); 
            img = images(:,:,indSlice,v); 
            fgt = figure; montageArray(lbl,cmap);
            fcnn= figure; montageArray(res,cmap);
            export_fig(fullfile(savePath,'figures',['montage_gt'  num2str(v)]),'-pdf','-transparent','-r300',fgt);
            export_fig(fullfile(savePath,'figures',['montage_cnn' num2str(v)]),'-pdf','-transparent','-r300',fcnn);            
        end
    otherwise
end
warning on;












function data = loadData()
paths    = setPaths;
nFiles   = 18; nChannelsPerVolume = 145; % 18 is the number of subjects
insz     = [158,123]; % dimensions of the original data
images   = zeros(insz(1),insz(2),nChannelsPerVolume,nFiles, 'uint8');
labels   = zeros(insz(1),insz(2),nChannelsPerVolume,nFiles, 'uint8');
tmpSeg   = zeros(insz(1),insz(2),nChannelsPerVolume, 'uint8');
imgFiles = dir(fullfile(paths.isbr_new.images,'*.mhd'));
segFiles = dir(fullfile(paths.isbr_new.labels,'*.mhd'));
assert(numel(imgFiles) == numel(segFiles))
isbrLabels = [0,2,3,4,5,7,8,10,11,12,13,14,15,16,17,18,24,26,28,29,30,41,...
    42,43,44,46,47,48,49,50,51,52,53,54,58,60,61,62,72];
labelMap = containers.Map(isbrLabels,0:numel(isbrLabels)-1);
ticStart = tic;
for i=1:nFiles
    img = read_mhd(fullfile(paths.isbr_new.images, imgFiles(i).name));
    seg = read_mhd(fullfile(paths.isbr_new.labels, segFiles(i).name));
    assert(size(img.data,3) == nChannelsPerVolume)
    tmpSeg(:) = 0;
    for j=1:numel(isbrLabels)
        tmpSeg(seg.data == isbrLabels(j)) = labelMap(isbrLabels(j));
    end
    images(:,:,:,i) = img.data;
    labels(:,:,:,i) = tmpSeg;
    cnnres(i)       = ImageType(img); % this is where the mhd data are stored
    progress('Reading ISBR images',i,nFiles,ticStart);
end

data.images = images;
data.labels = labels;
data.cnnres = cnnres;