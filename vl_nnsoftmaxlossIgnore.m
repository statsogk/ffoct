function Y = vl_nnsoftmaxlossIgnore(X,c,dzdy)
% VL_NNSOFTMAXLOSS  CNN combined softmax and logistic loss
%    Y = VL_NNSOFTMAX(X, C) applies the softmax operator followed by
%    the logistic loss the data X. X has dimension H x W x D x N,
%    packing N arrays of W x H D-dimensional vectors.
%
%    C contains the class labels, which should be integer in the range
%    1 to D.  C can be an array with either N elements or with H x W x
%    1 x N dimensions. In the fist case, a given class label is
%    applied at all spatial locations; in the second case, different
%    class labels can be specified for different locations.
%
%    D can be thought of as the number of possible classes and the
%    function computes the softmax along the D dimension. Often W=H=1,
%    but this is not a requirement, as the operator is applied
%    convolutionally at all spatial locations.
%
%    DZDX = VL_NNSOFTMAXLOSS(X, C, DZDY) computes the derivative DZDX
%    of the CNN with respect to the input X given the derivative DZDY
%    with respect to the block output Y. DZDX has the same dimension
%    as X.

% Copyright (C) 2014 Andrea Vedaldi.
% All rights reserved.
%
% This file is part of the VLFeat library and is made available under
% the terms of the BSD license (see the COPYING file).

assert(all(c(:) <= 255))
sz = [size(X,1) size(X,2) size(X,3) size(X,4)] ;

gtMasks = false(sz);
for i=1:sz(3), gtMasks(:,:,i,:) = c == i; end;
validLabels = c ~= 255;
assert(isequal(sz,size(gtMasks)))
assert(allvec((sum(gtMasks,3)==0) == ~validLabels))
% compute softmaxloss
Xmax = max(X,[],3) ;
ex = exp(bsxfun(@minus, X, Xmax)) ;

n = sz(1)*sz(2) ;
if nargin <= 2
    t = (Xmax + log(sum(ex,3))) .* validLabels - sum(X .* double(gtMasks),3) ;
    Y = sum(t(:)) / n ;
else
    Y = bsxfun(@rdivide, ex, sum(ex,3));
    assert(gather(all(isin01(Y(:)))));
    assert(gather(~anyvec(isnan(ex))))
    assert(gather(allvec(sum(ex,3))))
    assert(gather(~anyvec(isnan(Y))))
    Y(gtMasks) = Y(gtMasks) - 1;
    Y = (Y .* double(gtMasks)) * (dzdy / n) ;
end
