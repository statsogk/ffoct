function [cm,tp,fp,fn,acc] = evaluateFFOCT()

% Initialize paths and network
paths      = setPaths();
modelFile  = fullfile(paths.models, 'vgg128_noup_pool3_train_ffoct_iter_6000.caffemodel');
modelProto = fullfile(paths.protos,'matcaffe_ffoct.prototxt');
useGpu     = 1; matcaffe_init(useGpu,modelProto,modelFile);

% Read test images
labelMaps = dir(fullfile(paths.labels,'*.png'));
images    = dir(fullfile(paths.images,'*.tif'));
labelMaps = labelMaps(81:end);
images    = images(81:end); % these are the images we use for testing
patchSize = [321 321]; 
nLabels   = 3; 
cm        = zeros(nLabels);
tp        = zeros(nLabels,numel(images));
fp        = zeros(nLabels,numel(images));
fn        = zeros(nLabels,numel(images));
acc       = zeros(2,numel(images));
ticStart  = tic;
for i=1:numel(images)
    labels = imread(fullfile(paths.labels,labelMaps(i).name));
    if allvec(labels == 255), continue; end
    im     = preprocess(imread(fullfile(paths.images,images(i).name)));
    imPatches    = im2patches(im,patchSize);
    labelPatches = im2patches(labels,patchSize);
    
    % Throw away all patches that are fully unlabelled
    unlabelled   = squeeze(all(all(labelPatches == 255)));
    imPatches    = imPatches(:,:,~unlabelled);
    labelPatches = labelPatches(:,:,~unlabelled);
    
    % We assume that the unlabelled regions in the remaining patches are
    % healthy tissue (background)
    labelPatches(labelPatches == 255) = 0;
    for iPatch=1:size(imPatches,3)
        gt       = labelPatches(:,:,iPatch);
        scores   = matcaffe(imPatches(:,:,iPatch),useGpu);
        [~,cancerLabels] = max(scores,[],3); 
        cancerLabels = imresize(uint8(cancerLabels - 1),patchSize,'nearest');
        for j=1:nLabels
            cm(j,:) = cm(j,:) + histc(cancerLabels(gt == j-1)',0:nLabels-1);
            tp(j,i) = tp(j,i) + nnz(cancerLabels == j-1 & gt == j-1);
            fp(j,i) = fp(j,i) + nnz(cancerLabels == j-1 & gt ~= j-1);
            fn(j,i) = fn(j,i) + nnz(cancerLabels ~= j-1 & gt == j-1);
        end
        acc(1,i) = acc(1,i) + nnz(cancerLabels == gt);
        acc(2,i) = acc(2,i) + nnz(cancerLabels ~= gt);
    end
    progress('Evaluating segmentation results on FFOCT dataset...',...
        i,numel(images),ticStart);
end



