#!/bin/sh

# General settings
CLASS=isbr
SET=train
PROJECT_DIR=/home/tsogkas/code/ffoct
PROTOS_DIR=${PROJECT_DIR}/protos
CAFFE_BIN=/home/tsogkas/code/deeplab-public/build/tools/caffe.bin
SOLVER_PROTO=${PROTOS_DIR}/solver_${CLASS}_${SET}.prototxt
MODEL_FINETUNE=${PROJECT_DIR}/models/vgg16_128.caffemodel
GPU_ID=0

# Training from scratch 
CMD="${CAFFE_BIN} train \
     --solver=${SOLVER_PROTO} \
     --gpu=${GPU_ID}"
echo Running ${CMD} && ${CMD}

# Finetuning
#CMD="${CAFFE_BIN} train \
#     --solver=${SOLVER_PROTO} \
#     --weights=${MODEL_FINETUNE} \
#     --gpu=${GPU_ID}"
#echo Running ${CMD} && ${CMD}

