%% Load net
% modelName = 'net-isbr-multichannel-epoch-48.mat';
% modelName = 'net-isbr-256-epoch-39.mat';
% modelName = 'net-isbr-rgb-epoch-76.mat';
% modelName = 'net-isbr-patches-epoch-33.mat';
% modelName = 'net-isbr-patches-64-epoch-20.mat';
modelName = 'net-isbr-new-256-epoch-26.mat';
load(fullfile('results','cnn',modelName))
net.layers(end) = [];
net = vl_simplenn_move(net,'gpu');

%% Load data
paths = setPaths();
i = 18;
if 0
    img = load_nii(fullfile(paths.isbr.data, ['isbr' num2str(i) '.nii']));
    csf = load_nii(fullfile(paths.isbr.data, ['isbr' num2str(i) '_1.nii']));
    gm  = load_nii(fullfile(paths.isbr.data, ['isbr' num2str(i) '_2.nii']));
    wm  = load_nii(fullfile(paths.isbr.data, ['isbr' num2str(i) '_3.nii']));
    img = uint8(img.img); csf = csf.img; gm  = gm.img; wm  = wm.img;
    csf(gm>0) = 2; csf(wm>0) = 3; labels = uint8(csf);
else
    % ISBR new
    tmpSeg   = zeros(158,123,145, 'uint8');
    isbrLabels = [0,2,3,4,5,7,8,10,11,12,13,14,15,16,17,18,24,26,28,30,41,42,...
        43,44,46,47,49,50,51,52,53,54,58,60,62];
    labelMap = containers.Map(isbrLabels,0:numel(isbrLabels)-1);
    img = read_mhd(fullfile(paths.isbr_new.images, ['IBSR_' num2str(i) '_ana_strip.mhd']));
    seg = read_mhd(fullfile(paths.isbr_new.labels, ['IBSR_' num2str(i) '_seg_ana.mhd']));
    assert(size(img.data,3) == 145)
    for j=1:numel(isbrLabels)
        tmpSeg(seg.data == isbrLabels(j)) = labelMap(isbrLabels(j));
    end
    labels = uint8(tmpSeg); img = uint8(img.data);
end

%% Run network
indIm = 0; 
if net.normalization.imageSize(3) > 1
    width = (net.normalization.imageSize(3)-1)/2;
    im = img(:,:,(indIm-width):(indIm+width));
else
    im = img(:,:,indIm); 
end
if isfield(net,'stride')
    im = im2patches(im,net.outputSize);
else
    im = imresize(im, net.normalization.imageSize(1:2),'nearest');
end
labels = imresize(labels, net.normalization.imageSize(1:2),'nearest');
sz = size(im); 
res = vl_simplenn(net, gpuArray(single(im)));
p = softmax(imresize(gather(res(end).x), sz(1:2), 'bilinear'));
[~,l] = max(p,[],3);
if isfield(net,'stride')
    l = patches2im(l,[256 256]);
end

cmap = jet(labelMap.Count);
figure; subplot(121); imshow(labels(:,:,indIm),cmap); title('Groundtruth labels');
subplot(122); imshow(l,cmap); title(modelName)

% opts.xstdUnary  = 3;
% opts.ystdUnary  = 3;
% opts.wUnary     = 3;
% opts.xstdBinary = 50;
% opts.ystdBinary = 50;
% opts.wBinary    = 5;
% opts.rgbStd     = 10;
% crf = denseCRF(im,p,10); 
% [~,lcrf] = max(crf,[],3);

%% Write results to mhd file
isbrLabels = [0,2,3,4,5,7,8,10,11,12,13,14,15,16,17,18,24,26,28,30,41,42,...
    43,44,46,47,49,50,51,52,53,54,58,60,62];
labelMap = containers.Map(isbrLabels,0:numel(isbrLabels)-1);
for subject = [14,15,16]
    img = read_mhd(fullfile(paths.isbr_new.images, ['IBSR_' num2str(subject) '_ana_strip.mhd']));
    im = imresize(img.data,[256 256],'nearest');
    res = zeros(158,123,145,'uint8');
    for slice=1:size(im,3)
        tmp = vl_simplenn(net, gpuArray(single(im(:,:,slice))));
        p = softmax(imresize(gather(tmp(end).x), [158,123],'bilinear'));
        [~,res(:,:,slice)] = max(p,[],3);
    end
    res = res - 1;
    for label = isbrLabels
        cnnres = ImageType(img);
        cnnres.data = res == labelMap(label);
        write_mhd(['IBSR_' num2str(subject) '_ana_strip_label_' num2str(label) '.mhd'],cnnres);
    end
end

