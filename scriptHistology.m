% We use the following labels:
% 0:    healthy tissue or colmod 
% 1:    cis
% 2:    lis
% 255:  unlabeled areas

paths = setPaths();

im = preprocess(imread('/home/tsogkas/code/ffoct/data/Image/2.tif'));
labels = imread('/home/tsogkas/code/ffoct/data/labels/2.png');

%%
patchSize    = [321 321] * 3;
labelPatches = im2patches(labels,patchSize);
imagePatches = im2patches(im,patchSize);
unlabelled = squeeze(all(all(labelPatches == 255)));
cancer     = squeeze(any(any(labelPatches == 1 | labelPatches == 2)));
background = squeeze(any(any(labelPatches == 0)));
labelPatches = labelPatches(:,:,cancer);
imagePatches = imagePatches(:,:,cancer);
imagePatches = repmat(reshape(imagePatches,patchSize(1),patchSize(2),1,[]),[1 1 3 1]);
figure; 
subplot(121); montage(imagePatches); 
subplot(122); montage(reshape(double(labelPatches)/4,patchSize(1),patchSize(2),1,[])); colormap(jet(4))



