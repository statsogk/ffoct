function buildGroundtruth(dataset,part)

if nargin < 1, dataset = 'isbr'; end
if nargin < 2, part = {'full','patches','lists'}; end

switch dataset
    case 'ffoct'
        buildGroundtruthFFOCT(part)
    case 'isbr'
        buildGroundtruthIsbr(part)
    otherwise
        error('Unknown dataset')
end


%--------------------------------------------------------------------------
function buildGroundtruthFFOCT(part)
%--------------------------------------------------------------------------
if ismember('full',part),    saveLabelMapsFFOCT();  end
if ismember('patches',part), savePatchesFFOCT();    end
if ismember('lists',part),   saveFileListsFFOCT();  end

%--------------------------------------------------------------------------
function buildGroundtruthIsbr(part)
%--------------------------------------------------------------------------
if ismember('full',part),    saveLabelMapsIsbr();  end
if ismember('lists',part),   saveFileListsIsbr();  end

%--------------------------------------------------------------------------
function saveLabelMapsIsbr()
%--------------------------------------------------------------------------
% Save full label maps for ffoct images. We use the following labels:
% 0: background
% 1: CSF
% 2: GM
% 3: WM

% Read original patches
paths  = setPaths(); 
mkdir(fullfile(paths.isbr.images, 'train')); 
mkdir(fullfile(paths.isbr.labels, 'train'));
mkdir(fullfile(paths.isbr.images, 'val'))
mkdir(fullfile(paths.isbr.labels, 'val'))
nFiles = 13; nChannelsPerVolume = 128;
images = zeros(256,256,nChannelsPerVolume*nFiles, 'uint8');
labels = zeros(256,256,nChannelsPerVolume*nFiles, 'uint8');
ticStart = tic;
for i=1:nFiles
    img = load_nii(fullfile(paths.isbr.data, ['isbr' num2str(i) '.nii']));
    csf = load_nii(fullfile(paths.isbr.data, ['isbr' num2str(i) '_1.nii']));
    gm  = load_nii(fullfile(paths.isbr.data, ['isbr' num2str(i) '_2.nii']));
    wm  = load_nii(fullfile(paths.isbr.data, ['isbr' num2str(i) '_3.nii']));
    img = img.img; csf = csf.img; gm  = gm.img; wm  = wm.img;
    inds= ((i-1)*nChannelsPerVolume+1):(i*nChannelsPerVolume);
    csf(gm>0) = 2; csf(wm>0) = 3;
    images(:,:,inds) = img;
    labels(:,:,inds) = csf;
    progress('Reading images and labels from .nii files...',i,nFiles,ticStart,10);
end

clear csf img gm wm
nValFiles = 3; nTrainFiles = nFiles - nValFiles; 
imagesTrain = images(:,:,1:nTrainFiles*nChannelsPerVolume);
imagesVal   = images(:,:,(nTrainFiles*nChannelsPerVolume+1):end); clear images;
labelsTrain = labels(:,:,1:nTrainFiles*nChannelsPerVolume);
labelsVal   = labels(:,:,(nTrainFiles*nChannelsPerVolume+1):end); clear labels;
assert(size(imagesTrain,3) == nTrainFiles * nChannelsPerVolume);
assert(size(imagesVal  ,3) == nValFiles * nChannelsPerVolume);

% Augment patches
% Flip horizontally
imagesTrain = cat(3, imagesTrain, flipdim(imagesTrain, 2));
labelsTrain = cat(3, labelsTrain, flipdim(labelsTrain, 2));

% Shift images
shift = 20;
imagesTrain = cat(3, circshift(imagesTrain, [shift 0]), circshift(imagesTrain, [-shift 0]), ...
                circshift(imagesTrain, [0 shift]),      circshift(imagesTrain, [0 -shift]),...
                circshift(imagesTrain, [shift shift]),  circshift(imagesTrain, [-shift -shift]),...
                circshift(imagesTrain, [shift -shift]), circshift(imagesTrain, [-shift shift]));
labelsTrain = cat(3, circshift(labelsTrain, [shift 0]), circshift(labelsTrain, [-shift 0]), ...
                circshift(labelsTrain, [0 shift]),      circshift(labelsTrain, [0 -shift]),...
                circshift(labelsTrain, [shift shift]),  circshift(labelsTrain, [-shift -shift]),...
                circshift(labelsTrain, [shift -shift]), circshift(labelsTrain, [-shift shift]));

labelsTrain = reshape(imresize(labelsTrain, [321 321], 'nearest'),  321,321,1,[]);
labelsVal   = reshape(imresize(labelsVal,   [321 321], 'nearest'),  321,321,1,[]);
imagesTrain = reshape(imresize(imagesTrain, [321 321], 'bilinear'), 321,321,1,[]);
imagesVal   = reshape(imresize(imagesVal,   [321 321], 'bilinear'), 321,321,1,[]);
assert(allvec(labelsTrain >=0 & labelsTrain <= 4))
assert(allvec(labelsVal >=0 & labelsVal <= 4))
assert(size(imagesTrain,4) == size(labelsTrain,4))
assert(size(imagesVal,4) == size(labelsVal,4))
for i=1:size(imagesTrain,4)
    imwrite(imagesTrain(:,:,:,i), fullfile(paths.isbr.images, 'train', ['image_' num2str(i) '.jpg']));
    imwrite(labelsTrain(:,:,:,i), fullfile(paths.isbr.labels, 'train', ['label_' num2str(i) '.png']));
end
for i=1:size(imagesVal,4)
    imwrite(imagesVal(:,:,:,i), fullfile(paths.isbr.images, 'val', ['image_' num2str(i) '.jpg']));
    imwrite(labelsVal(:,:,:,i), fullfile(paths.isbr.labels, 'val', ['label_' num2str(i) '.png']));
end



%--------------------------------------------------------------------------
function saveLabelMapsFFOCT()
%--------------------------------------------------------------------------
% Save full label maps for ffoct images. We use the following labels:
% 0: colmod and background (healthy tissue)
% 1: cis type of cancer
% 2: lis type of cancer

paths = setPaths();
im    = dir(fullfile(paths.images,'*.tif'));
mkdir(paths.labels);

% Merge labels for each image and store them in labels directory
ticStart = tic;
for i=1:numel(im)
    imt = imread(fullfile(paths.images,im(i).name));
    [~,id]  = fileparts(im(i).name);
    bgpath  = fullfile(paths.background,[id '.labels-background.tif']);
    colpath = fullfile(paths.colmod,[id '.labels-background-colmod.tif']);
    cispath = fullfile(paths.cis,[id '.labels-cis.tif']);
    lispath = fullfile(paths.lis,[id '.labels-lis.tif']);
    labels  = 255*ones(size(imt),'uint8');
    if exist(bgpath,'file')
        tmp = imread(bgpath);
        labels(tmp > 0) = 0;
    end
    if exist(colpath,'file')
        tmp = imread(colpath);
        labels(tmp > 0) = 0;
    end
    if exist(cispath,'file')
        tmp = imread(cispath);
        labels(tmp > 0) = 1;
    end
    if exist(lispath,'file')
        tmp = imread(lispath);
        labels(tmp > 0) = 2;
    end
    imwrite(labels, jet(4), fullfile(paths.labels, [id '.png']));
    progress('Saving full label maps for FFOCT images...',i,numel(im),ticStart)
end


%--------------------------------------------------------------------------
function savePatchesFFOCT()
%--------------------------------------------------------------------------
% Crop and save patches from images and labels maps to use as groundtruth.
paths = setPaths();
mkdir(paths.imagePatches, 'train'); % create folders to save groundtruth
mkdir(paths.imagePatches, 'test');
mkdir(paths.imagePatches, 'val');
mkdir(paths.labelPatches, 'train');
mkdir(paths.labelPatches, 'test');
mkdir(paths.labelPatches, 'val');
patchSize = [321 321]; scales = [1 2 3 4 5]; 
labelMaps = dir(fullfile(paths.labels,'*.png'));
images    = dir(fullfile(paths.images,'*.tif'));
ticStart  = tic; nTrainImages = 75;
for i=1:nTrainImages  % We use the first N images as the train set
    [~,id]        = fileparts(images(i).name);
    labels        = imread(fullfile(paths.labels,labelMaps(i).name));
    if allvec(labels == 255), continue; end  % skip image if unlabelled
    im            = preprocess(imread(fullfile(paths.images,images(i).name)));
    boxes         = [];
    for sc = scales
        boxes     = [boxes; getGridBoxes(labels,patchSize*sc)];
    end
    [h,w] = bb2hw(boxes); 
    assert(all(h>=1 & h <= size(im,1))); assert(all(w>=1 & w <= size(im,2)))
    labelPatches  = boxes2patches(labels, boxes, patchSize,'nearest');
    
    % Remove boxes that do not contain any labels. 
    unlabelled   = squeeze(all(all(labelPatches == 255))); 
    boxes        = boxes(~unlabelled,:);
    labelPatches = labelPatches(:,:,~unlabelled);
    
    % Augment boxes that contain positives
    containsCancer = squeeze(any(any(labelPatches == 1 | labelPatches == 2)));
    boxes = [boxes(~containsCancer,:); augmentBoxes(boxes(containsCancer,:), size(im))];
    
    % We do not do dataset augmentation
    imagePatches = boxes2patches(im, boxes, patchSize, 'bilinear');
    labelPatches = boxes2patches(labels, boxes, patchSize, 'nearest');
    imagePatches = reshape(imagePatches,patchSize(1),patchSize(2),1,[]);
%     imagePatches = repmat(imagePatches, [1 1 3 1]); % turn to RGB for training 
%     assert(size(imagePatches,3) == 3)
    assert(size(imagePatches,4) == size(labelPatches,3));
    
    %figure(1); 
    %subplot(121); montage(imagePatches); title('Image patches');
    %subplot(122); montage(reshape(double(labelPatches)/4,patchSize(1),patchSize(2),1,[])); 
    %title('Label patches'); colormap(jet(4));

    % Save patches to disk (maybe move this inside boxes2patches)
    cmap = jet(4);
    for j=1:size(boxes,1)
        if i <= 70
            set = 'train';
        else
            set = 'val';
        end
        boxString = sprintf('_%d_%d_%d_%d',boxes(j,1),boxes(j,2),boxes(j,3),boxes(j,4));
        imwrite(labelPatches(:,:,j),  cmap, fullfile(paths.labelPatches, set, [id boxString '.png']));
        imwrite(imagePatches(:,:,:,j),fullfile(paths.imagePatches, set, [id boxString '.jpg']));
    end 
    msg = sprintf('Generating groundtruth patches (%d FG pixels , %d BG pixels)...',...
        nnz(labelPatches > 0 & labelPatches < 255), nnz(labelPatches == 0)); 
    progress(msg,i,nTrainImages,ticStart);
end


%--------------------------------------------------------------------------
function savePatches2()
%--------------------------------------------------------------------------
% Crop and save patches from images and labels maps to use as groundtruth.
paths = setPaths();
mkdir(paths.imagePatches, 'train'); % create folders to save groundtruth
mkdir(paths.imagePatches, 'test');
mkdir(paths.labelPatches, 'train');
mkdir(paths.labelPatches, 'test');
patchSize = [321 321];
labelMaps = dir(fullfile(paths.labels,'*.png'));
images    = dir(fullfile(paths.images,'*.tif'));
ticStart  = tic; nTrainImages = 70;
for i=1:nTrainImages  % We use the first N images as the train set
    [~,id]        = fileparts(images(i).name);
    labels        = imread(fullfile(paths.labels,labelMaps(i).name));
    if allvec(labels == 255), continue; end  % skip image if unlabelled
    im            = preprocess(imread(fullfile(paths.images,images(i).name)));
    boxes         = getGridBoxes(labels,patchSize);
    labelPatches  = boxes2patches(labels, boxes, patchSize,'nearest');
    
    % Remove boxes that do not contain any labels. 
    unlabelled   = squeeze(all(all(labelPatches == 255))); 
    boxes        = boxes(~unlabelled,:);
    labelPatches = labelPatches(:,:,~unlabelled);

    % For cancer patches, we consider that the unlabelled area around
    % positive pixels is background (healthy tissue). This is a simplifying
    % but relatively safe assumption to make: doctors would have most 
    % probably labelled cancerous tissue that is very close to already
    % labelled regions.
%     labelPatches(labelPatches == 255) = 0;

    
    % Augment remaining bounding boxes and extract final patches. We do not 
    % augment boxes that contain only background to keep the training set
    % at a reasonable size.
    background   = squeeze(all(all(labelPatches == 0 | labelPatches == 255))); 
    boxesBG      = boxes(background,:);
    boxesFG      = augmentBoxes(boxes(~background,:),size(im));
    boxes        = [boxesFG; boxesBG];
    labelPatches = boxes2patches(labels, boxes, patchSize, 'nearest');
    imagePatches = boxes2patches(im, boxes, patchSize, 'bilinear');
    imagePatches = reshape(imagePatches,patchSize(1),patchSize(2),1,[]);
    imagePatches = repmat(imagePatches, [1 1 3 1]); % turn to RGB for training 
    assert(size(imagePatches,4) == size(labelPatches,3));
    assert(size(imagePatches,3) == 3)

    % Save patches to disk (maybe move this inside boxes2patches)
    for j=1:size(boxes,1)
        boxString = sprintf('_%d_%d_%d_%d',boxes(j,1),boxes(j,2),boxes(j,3),boxes(j,4));
        imwrite(labelPatches(:,:,j),  fullfile(paths.labelPatches, 'train', [id boxString '.png']));
        imwrite(imagePatches(:,:,:,j),fullfile(paths.imagePatches, 'train', [id boxString '.jpg']));
    end 
    msg = sprintf('Generating groundtruth patches (%d FG boxes, %d BG boxes)...',...
        size(boxesFG,1), size(boxesBG,1)); 
    progress(msg,i,nTrainImages,ticStart);
end


%--------------------------------------------------------------------------
function savePatches3()
%--------------------------------------------------------------------------
% Crop and save patches from images and labels maps to use as groundtruth.
paths = setPaths();
mkdir(paths.imagePatches, 'train'); % create folders to save groundtruth
mkdir(paths.imagePatches, 'test');
mkdir(paths.labelPatches, 'train');
mkdir(paths.labelPatches, 'test');
patchSize = [321 321];
labelMaps = dir(fullfile(paths.labels,'*.png'));
images    = dir(fullfile(paths.images,'*.tif'));
ticStart  = tic; nTrainImages = 80;
for i=1:nTrainImages  % We use the first N images as the train set
    [~,id]        = fileparts(images(i).name);
    labels        = imread(fullfile(paths.labels,labelMaps(i).name));
    if allvec(labels == 255), continue; end  % skip image if unlabelled
    im            = preprocess(imread(fullfile(paths.images,images(i).name)));
    boxesCancer   = getCancerBoxes(labels);
    boxesHealthy  = getGridBoxes(labels);
    patchesHealthy= boxes2patches(labels, boxesHealthy, patchSize,'nearest');
    patchesCancer = boxes2patches(labels, boxesCancer,  patchSize,'nearest');
    
    % Remove boxes that contain no labels from healthy patches
    unlabelled    = squeeze(all(all(patchesHealthy == 255))); 
    boxesHealthy  = boxesHealthy(~unlabelled,:);
    patchesHealthy= patchesHealthy(:,:,~unlabelled);
    
    % For cancer patches, we consider that the unlabelled area around
    % positive pixels is background (healthy tissue). This is a simplifying
    % but relatively safe assumption to make: doctors would have most 
    % probably labelled cancerous tissue that is very close to already
    % labelled regions.
    patchesCancer(patchesCancer == 255) = 0;
    
    % Stack boxes and patches
    boxes        = [boxesCancer; boxesHealthy];
    labelPatches = cat(3, patchesCancer, patchesHealthy);
    imagePatches = boxes2patches(im,boxes,patchSize,'bilinear');
    imagePatches = reshape(imagePatches,patchSize(1),patchSize(2),1,[]);
    imagePatches = repmat(imagePatches, [1 1 3 1]); % turn to RGB for training 
    assert(size(imagePatches,4) == size(labelPatches,3));
    assert(size(imagePatches,3) == 3)

    % Save patches to disk (maybe move this inside boxes2patches)
    for j=1:size(boxes,1)
        boxString = sprintf('_%d_%d_%d_%d',boxes(j,1),boxes(j,2),boxes(j,3),boxes(j,4));
        imwrite(labelPatches(:,:,j),  fullfile(paths.labelPatches, 'train', [id boxString '.png']));
        imwrite(imagePatches(:,:,:,j),fullfile(paths.imagePatches, 'train', [id boxString '.jpg']));
    end 
    progress('Generating groundtruth patches from FFOCT images...',i,nTrainImages,ticStart)
end

%--------------------------------------------------------------------------
function saveFileListsFFOCT()
%--------------------------------------------------------------------------
% Create .txt file with the image and segmentations filenames 
paths        = setPaths();
gtImageNames = dir(fullfile(paths.imagePatches, 'train', '*.jpg'));
gtSegNames   = dir(fullfile(paths.labelPatches, 'train', '*.png'));
fp = fopen(fullfile(paths.lists,['list_ffoct_train' '.txt']),'w');
for i=1:numel(gtImageNames)
    fprintf(fp,'%s %s\n',...
        fullfile('/data/image_patches/train/', gtImageNames(i).name),...
        fullfile('/data/label_patches/train/', gtSegNames(i).name));
end
fclose(fp);

%--------------------------------------------------------------------------
function saveFileListsIsbr()
%--------------------------------------------------------------------------
% Create .txt file with the image and segmentations filenames 
paths        = setPaths();
gtImageNames = dir(fullfile(paths.isbr.images, 'train', '*.jpg'));
gtSegNames   = dir(fullfile(paths.isbr.labels, 'train', '*.png'));
fp = fopen(fullfile(paths.lists,['list_isbr_train' '.txt']),'w');
for i=1:numel(gtImageNames)
    fprintf(fp,'%s %s\n',...
        fullfile('/data/isbr_images/train/', gtImageNames(i).name),...
        fullfile('/data/isbr_labels/train/', gtSegNames(i).name));
end
fclose(fp);

%--------------------------------------------------------------------------
function patches = boxes2patches(im,bb,patchSize, method)
%--------------------------------------------------------------------------
% Extract image patches given their bounding boxes. 
% Patches are optionally resized to a pre-defined size.
if isempty(bb), patches = []; return; end
nBoxes = size(bb,1);
if nargin > 2
    assert(numel(patchSize == 2) && isnumeric(patchSize),...
        'PatchSize must be a 2-element vector');
    patches = zeros([patchSize nBoxes],'like',im);
    for i=1:nBoxes
        p = im(bb(i,2):bb(i,4),bb(i,1):bb(i,3));
        if ~isequal(size(p),patchSize)
            p = imresize(p,patchSize,method);
        end
        patches(:,:,i) = p;
    end 
else
    patches = cell(nBoxes,1);
    for i=1:nBoxes
        patches{i} = im(bb(i,2):bb(i,4),bb(i,1):bb(i,3));
    end 
end

%--------------------------------------------------------------------------
function boxes = getCancerBoxes(labelMap)
%--------------------------------------------------------------------------
% get bounding boxes for connected components for labels 1,2 (cancer)
cc = bwconncomp(labelMap == 1 | labelMap == 2);
bb = augmentBoxes(cc2boxes(cc),cc.ImageSize);
boxes = []; boxes = [boxes; bb];

%--------------------------------------------------------------------------
function boxes = getGridBoxes(labelMap,patchSize)
%--------------------------------------------------------------------------
% get bounding boxes for connected components for label 0 (healthy). Since
% the spatial support for healthy tissues is much larger than for cancer,
% we sample patches from a rectangular grid.
imsize = size(labelMap);
[x,y] = meshgrid(1:patchSize(2):imsize(2), 1:patchSize(1):imsize(1));
boxes = [x(:), y(:), x(:)+patchSize(2)-1, y(:)+patchSize(1)-1];
boxes(:,3) = min(boxes(:,3),imsize(2)); 
boxes(:,4) = min(boxes(:,4),imsize(1));
assert(allvec(boxes(:,[1,3]) >= 1 & boxes(:,[1,3]) <= imsize(2)))
assert(allvec(boxes(:,[2,4]) >= 1 & boxes(:,[2,4]) <= imsize(1)))
[h,w] = bb2hw(boxes); 
assert(all(h >=1 & h <= patchSize(1)));
assert(all(w >=1 & w <= patchSize(2)));
% outOfBounds = boxes(:,3) > imsize(2) | boxes(:,4) > imsize(1);
% boxes(outOfBounds,:) = [];

%--------------------------------------------------------------------------
function boxes = augmentBoxes(bb,imsize)
%--------------------------------------------------------------------------
% apply several transformations on the original bounding boxes 

boxes = bb;
if isempty(bb), return; end

% 1. increase context area around original bounding box by some percentage
bbContext = [];
padperc = [0.05, 0.15, 0.25];
[h,w] = bb2hw(bb);
for i=1:numel(padperc)
    hnew = round(padperc(i)*h);
    wnew = round(padperc(i)*w);
    bbnew= bb + [-wnew, -hnew, wnew, hnew];
    bbContext = [bbContext; bbnew];
end

% 2. Create translated versions of boxes
bbTrans = [];
[h,w] = bb2hw(boxes);
stepx = round(0.1*w);
stepy = round(0.1*h);
nTrans= 2;  % number of left/right/top/down translations
for i=-nTrans:nTrans
    for j=-nTrans:nTrans
        bbnew   = bb + [i*stepx,j*stepy,i*stepx,j*stepy];
        bbTrans = [bbTrans; bbnew];
    end
end
boxes = [bbContext; bbTrans];

% Remove invalid and overlapping boxes
[h,w] = bb2hw(boxes); minSize = 20;
boxes(:,1) = max(boxes(:,1), 1); 
boxes(:,2) = max(boxes(:,2), 1);
boxes(:,3) = min(boxes(:,3), imsize(2)); 
boxes(:,4) = min(boxes(:,4), imsize(1));
small = h < minSize | w < minSize;
boxes(small,:) = [];
boxes = unique(boxes,'rows');

%--------------------------------------------------------------------------
function [h,w] = bb2hw(bb)
%--------------------------------------------------------------------------
% calculate height and widths of bounding boxes
% bounding boxes are assumed to be in the form [xmin,ymin,xmax,ymax]
h = bb(:,4)-bb(:,2)+1;
w = bb(:,3)-bb(:,1)+1;

%--------------------------------------------------------------------------
function boxes = cc2boxes(cc)
%--------------------------------------------------------------------------
% get bounding boxes for each connected component
boxes = [];
for i=1:cc.NumObjects
    if numel(cc.PixelIdxList{i}) > 10
        boxes = [boxes; indexes2bbox(cc.PixelIdxList{i},cc.ImageSize)];
    end
end

%--------------------------------------------------------------------------
function bb = indexes2bbox(inds, imsize)
%--------------------------------------------------------------------------
% turns linear indexes of a region to its bounding box coordinates
[y,x] = ind2sub(imsize,inds);
bb = [min(x),min(y),max(x),max(y)];

