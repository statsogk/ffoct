function diceCoeff = testISBR()

% use the correct model names
data     = loadData();
net1_6   = loadNet('net-isbr-new-test1-6-epoch-22.mat');
net7_12  = loadNet('net-isbr-new-test7-12-epoch-22.mat');
net13_18 = loadNet('net-isbr-new-test13-18-epoch-22.mat');

% Run network
paths = setPaths;
isbrLabels = [0,2,3,4,5,7,8,10,11,12,13,14,15,16,17,18,24,26,28,29,30,41,...
    42,43,44,46,47,48,49,50,51,52,53,54,58,60,61,62,72];
labelMap = containers.Map(isbrLabels,0:numel(isbrLabels)-1);
nLabels  = numel(isbrLabels);
nVolumes = size(data.images,4);
diceCoeff = zeros(nVolumes,nLabels);
ticStart = tic;
for v=1:nVolumes;
    if v <= 6 % choose the correct network for each testing example
        net = net1_6;
    elseif v <= 12
        net = net7_12;
    else
        net = net13_18;
    end
    nSlicesPerVolume = 145; 
    insz = [158,123,nSlicesPerVolume];
    slices = imresize(data.images(:,:,:,v), net.normalization.imageSize(1:2),'nearest');
    labels = imresize(data.labels(:,:,:,v), insz(1:2),'nearest');
    if gpuDeviceCount
        slices = gpuArray(single(slices)); % move volume to gpu
    else
        slices = single(slices);
    end
    res = zeros(insz,'uint8');
    for s=1:nSlicesPerVolume 
        tmp = vl_simplenn(net, slices(:,:,s));
        p = softmax(imresize(gather(tmp(end).x), insz(1:2), 'bilinear'));
        [~,res(:,:,s)] = max(p,[],3);
    end
    res = res - 1;
    for l=1:nLabels % first label should be the background
        label = labelMap(isbrLabels(l));
        data.cnnres(v).data = res == label;
        diceCoeff(v,l) = dice(res == label, labels == label);
        write_mhd(fullfile(paths.results, 'cnn','isbr',...
            ['IBSR_' num2str(v) '_ana_strip_label_' num2str(isbrLabels(l)) '.mhd']),data.cnnres(v));
    end
    save(fullfile(paths.results, 'cnn','isbr',['IBSR_res' num2str(v)  '.mat']),'res')
    progress(['Testing on volume ' num2str(v)],v,nVolumes,ticStart);
end


function data = loadData()
paths    = setPaths;
nFiles   = 18; nChannelsPerVolume = 145; % 18 is the number of subjects
insz     = [158,123]; % dimensions of the original data
images   = zeros(insz(1),insz(2),nChannelsPerVolume,nFiles, 'uint8');
labels   = zeros(insz(1),insz(2),nChannelsPerVolume,nFiles, 'uint8');
tmpSeg   = zeros(insz(1),insz(2),nChannelsPerVolume, 'uint8');
imgFiles = dir(fullfile(paths.isbr_new.images,'*.mhd'));
segFiles = dir(fullfile(paths.isbr_new.labels,'*.mhd'));
assert(numel(imgFiles) == numel(segFiles))
isbrLabels = [0,2,3,4,5,7,8,10,11,12,13,14,15,16,17,18,24,26,28,29,30,41,...
    42,43,44,46,47,48,49,50,51,52,53,54,58,60,61,62,72];
labelMap = containers.Map(isbrLabels,0:numel(isbrLabels)-1);
ticStart = tic;
for i=1:nFiles
    img = read_mhd(fullfile(paths.isbr_new.images, imgFiles(i).name));
    seg = read_mhd(fullfile(paths.isbr_new.labels, segFiles(i).name));
    assert(size(img.data,3) == nChannelsPerVolume)
    tmpSeg(:) = 0;
    for j=1:numel(isbrLabels)
        tmpSeg(seg.data == isbrLabels(j)) = labelMap(isbrLabels(j));
    end
    images(:,:,:,i) = img.data;
    labels(:,:,:,i) = tmpSeg;
    cnnres(i)       = ImageType(img); % this is where the mhd data are stored
    progress('Reading ISBR images',i,nFiles,ticStart);
end

data.images = images;
data.labels = labels;
data.cnnres = cnnres;

function net = loadNet(modelName)
paths = setPaths();
load(fullfile(paths.results,'cnn',modelName))
net.layers(end) = [];
if gpuDeviceCount
    net = vl_simplenn_move(net,'gpu');
end

function res = dice(a,b)
a = logical(a);
b = logical(b);
res = 2*nnz(a & b)/max(1,nnz(a)+nnz(b));
